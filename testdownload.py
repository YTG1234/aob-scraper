#!/usr/bin/env python3

from aob_scraper.bwv import play_or_dl
from aob_scraper.dl import download
from aob_scraper.all import search

download(["aobDl", "847", "--classifiers", "series", "genre", "--format", "ogg"])
search(["aobAll", "", "--classifiers", "series", "genre"])
play_or_dl(["bwv", "847"])
