# For dependencies, arguments and envvars, see README.org.

from typing import Optional
from .utils import download_parser, env_args, not_empty, play_parser, system2, State, FileFormat, file_exists_anyf
from .dl    import download

import argparse

def play_or_dl(argv: list[str], unstrict: bool = False):
    parser = argparse.ArgumentParser(prog=argv[0], description="Download and/or play an AoB performance.", parents=[download_parser(), play_parser()])
    parser.add_argument("bwv", type=int, help="The BWV catalogue number", metavar="NUMBER")
    modifier_g = parser.add_mutually_exclusive_group()
    modifier_g.add_argument("--modifier", help="A little bit that comes after the number", default="")
    modifier_g.add_argument("--dash", help="A modifier preceded by a dash", default="")
    args = parser.parse_args([*env_args(), *argv[1:]])

    bwv: int = args.bwv
    classifiers: list[str] = args.classifiers
    dash: str = args.dash
    modifier: str = args.modifier
    modifierr: str = ("-" + dash) if not_empty(dash) else modifier
    extension: str = args.extension
    acodec: str = args.acodec
    vcodec: Optional[str] = args.vcodec if not_empty(args.vcodec) else None
    cut_seconds: float = args.cut
    player: str = args.player
    gui: bool = args.gui
    fullscreen: bool = args.fullscreen
    raise_by: float = args.raises

    # We want to check all file formats before proceeding to download.
    s = State(bwv, classifiers, modifierr, FileFormat(extension, acodec, vcodec), raise_by, False, cut_seconds)
    fname = file_exists_anyf(s)

    if not fname:
        args_for_dl = ["aobDl", str(bwv), "--classifiers", *classifiers, "--format", extension]
        if not_empty(modifier):
            args_for_dl.extend(["--modifier", modifier])
        if not_empty(dash):
            args_for_dl.extend(["--dash", dash])
        if not_empty(acodec):
            args_for_dl.extend(["--acodec", acodec])
        if not_empty(vcodec):
            args_for_dl.extend(["--vcodec", str(vcodec)])
        if args.cut != -1:
            args_for_dl.extend(["--cut", str(args.cut)])
        if args.raises:
            args_for_dl.extend(["--raise", str(args.raises)])
        if args.force:
            args_for_dl.append("--force")
        if args.no_download:
            args_for_dl.append("--no-download")
        download(args_for_dl, unstrict=unstrict)
        fname = s.file_name()
        if not fname: return

    if player == "ffplay":
        cmd = "ffplay -autoexit"
        if not gui and not fullscreen: cmd += " -nodisp"
        if fullscreen: cmd += " -fs"
    elif player == "mpv":
        cmd = "mpv"
        if gui: cmd += " --player-operation-mode=pseudo-gui"
        if fullscreen: cmd += " --fs"
    elif player == "aplay" or player == "afplay":
        cmd = player
    else:
        exit(1)
    cmd += " '" + fname + "'"
    system2("bwv:", cmd)
