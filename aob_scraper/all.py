# For dependencies, arguments and envvars see README.org.

import shutil
from typing		import Any, Tuple, Optional
from threading		import Lock, Thread
from urllib.parse       import quote_plus

from .utils		import download_parser, env_args, first, log, not_empty, play_parser
from .dl                import download
from .bwv               import play_or_dl

import requests, re, bs4, signal, argparse

BwvEntry = Tuple[int, str]

class AobAllDownloadThread(Thread):
    dir_list: list[str]
    args: list[str]
    lock: Lock

    def __init__(self, args: list[str], dir_list: list[str], lock: Lock):
        super().__init__()

        self.args = args
        self.dir_list = dir_list
        self.lock = lock

    def run(self):
        download(self.args, dir_list=self.dir_list, lock=self.lock, unstrict=True)

def parse_to_entry(inp: str, exclude_movement_only: bool) -> Optional[BwvEntry]:
    match = re.compile(r'/bwv-(\d+)([\w\-_]*)').search(inp)
    if match:
        value = (int(match.group(1)), match.group(2))
        if exclude_movement_only:
            return value if not match.group(2).startswith("-") else None
        return value

def parse_and_log(inp: str, exclude_movement_only: bool) -> Optional[BwvEntry]:
    maybe_entry = parse_to_entry(inp, exclude_movement_only)
    if maybe_entry: log("aobAll: parse_and_log:", f"Found and parsed an entry {maybe_entry}")
    return maybe_entry

def parse_html(url: str, exclude_movement_only: bool, acc: list[BwvEntry] = []) -> list[BwvEntry]:
    log("aobAll: parse_html:", "Parsing " + url)
    resp = requests.get(url)
    if resp.status_code < 200 or resp.status_code >= 400: return acc

    soup: Any = bs4.BeautifulSoup(resp.content, "html.parser")

    # The accumulator + new pieces
    feedacc = acc + [
        entry for entry in (
            parse_and_log(art.a.attrs["href"], exclude_movement_only) for art in soup.findAll(
                name="article"
            ) if art.a and art.a.attrs and "href" in art.a.attrs
        ) if entry != None
    ]

    # First finding of next page
    fst = first(
        lambda x: "rel" in x.attrs and "next" in x.attrs["rel"],
        soup.findAll(name="a")
    )

    if fst:
        log("aobAll: parse_html:", "Found next page!")
        href = fst.attrs["href"]
        if href.startswith("https://"): nextpage = href
        else:
            nextpage = "https://bachvereniging.nl" + href

        # Parse the next page
        return parse_html(
            url=nextpage, exclude_movement_only=exclude_movement_only,
            acc=feedacc
        )
    # Return the previously set value
    return feedacc

def args_for_entry(entry: BwvEntry, args: argparse.Namespace) -> list[str]:
    args_for_dl = [
        str(entry[0]),
        "--classifiers", *args.classifiers,
        "--raise", str(args.raises),
        "--format", args.extension, "--cut", str(args.cut)
    ]
    if not_empty(entry[1]):
        if entry[1].startswith("-"):
            args_for_dl.extend(["--dash", entry[1][1:]])
        else: args_for_dl.extend(["--modifier", entry[1]])
    if not_empty(args.acodec):
        args_for_dl.extend(["--acodec", args.acodec])
    if not_empty(args.vcodec):
        args_for_dl.extend(["--vcodec", args.vcodec])
    if args.force:
        args_for_dl.append("--force")
    if args.no_download:
        args_for_dl.append("--no-download")
    return args_for_dl

def search(argv: list[str]):
    parser = argparse.ArgumentParser(prog=argv[0], description="Download and/or play many AoB performances by query.", parents=[play_parser(), download_parser()])
    parser.add_argument("query", help="Search query for AoB")
    parser.add_argument("--filters", help="Filter options for the website (URL query structure)", nargs="+", type=str)
    parser.add_argument("--play", help="Play the items", action="store_true")
    parser.add_argument("--no-movement-only", help="Exclude non-full pieces", action="store_true")
    args = parser.parse_args([*env_args(), *argv[1:]])

    filters = ""
    if args.filters:
        for filter in args.filters:
            name, ids = filter.splpit("=")
            filters += f"&aob_facet_{name}_ids={quote_plus(ids)}"

    log("aobAll:", "Sending search request...")
    entries = parse_html(
        url="https://bachvereniging.nl/en/bwv/?keywords=" + quote_plus(args.query) + filters + "&sorted-by=bwv&sort_order=asc",
        exclude_movement_only=args.no_movement_only
    )

    log("aobAll: search:", "Downloading/playing all saved items...")
    if args.play:
        for entry in entries:
            log("aobAll: search:", f"({entry[0]}{entry[1]})")
            args_for_dl = [*args_for_entry(entry, args), "--player", args.player,]
            if args.gui:
                args_for_dl.append("--gui")
            if args.fullscreen:
                args_for_dl.append("--fullscreen")
            play_or_dl(["bwv", *args_for_dl], unstrict=True)
    else:
        dir_list: list[str] = []
        lock = Lock()
        threads: list[Thread] = []
        for entry in entries:
            args_for_dl = ["aobDl", *args_for_entry(entry, args)]
            threads.append(AobAllDownloadThread(args_for_dl, dir_list=dir_list, lock=lock))

        for t in threads: t.start()

        # Multi-threaded programming, lol
        def handler(_, __):
            with lock:
                for d in dir_list:
                    shutil.rmtree(d)
            exit(0)
        signal.signal(signal.SIGINT, handler)

        for t in threads: t.join()
