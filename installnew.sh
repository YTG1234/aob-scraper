#!/bin/sh -e

rm -rf dist
python3 -m build
pipx uninstall aob-scraper
pipx install --system-site-packages "$(printf '%s[ytdlp]' "$(find dist -type f -name "*.tar.gz")")"
